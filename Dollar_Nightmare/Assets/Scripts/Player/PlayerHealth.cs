﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
    Rigidbody playerRigidbody;
   

    //darkness slider variables
    public Slider lightSlider;
    public float timer;
    public float timerMax = 10;
    public Image darkImage;
    public Color dark1 = new Color(0f, 0f, 0f, 0.25f);
    public Color dark2 = new Color(0f, 0f, 0f, 0.5f);
    public Color dark3 = new Color(0f, 0f, 0f, 0.75f);
    public Color clear = new Color(0f, 0f, 0f, 0f);

    Animator anim;
    AudioSource playerAudio;
    PlayerMovement playerMovement;
    //PlayerShooting playerShooting;
    bool isDead;
    bool damaged;
    bool inLight = false;


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        playerAudio = GetComponent <AudioSource> ();
        playerMovement = GetComponent <PlayerMovement> ();
        //playerShooting = GetComponentInChildren <PlayerShooting> ();
        currentHealth = startingHealth;
        timer = timerMax;
    }


    void Update ()
    {
        if(damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;
        //Set slider value to timer
        
        lightSlider.value = timer;


        if (timer <= 0 && inLight == false && !isDead)
        {
            Death();
        }
        //if they arent in the light trigger count down time
        else if(inLight == false)
        {
            timer -= Time.deltaTime;
            if(timer <= 7.5 && timer >= 5)
            {
                darkImage.color = dark1;
            }
            else if (timer <= 5 && timer >= 2.5)
            {
                darkImage.color = dark2;
            }
            else if (timer <= 2.5 && timer >= 0)
            {
                darkImage.color = dark3;
            }
           
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Light"))
        {
            timer = 10;
            inLight = true;
            darkImage.color = clear;
        }
        if (other.gameObject.CompareTag("Poop"))
        {
            damaged = true;
            currentHealth -= 5;
            healthSlider.value = currentHealth;

            playerAudio.Play();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Light"))
        {
            inLight = false;
        }
    }


    public void TakeDamage (int amount)
    {
        //playerRigidbody.constraints = RigidbodyConstraints.FreezePositionY;
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;

        playerAudio.Play ();

        if(currentHealth <= 0 && !isDead)
        {
            Death ();
        }
    }
  

    void Death ()
    {
        isDead = true;

        //playerShooting.DisableEffects ();

        anim.SetTrigger ("Die");

        playerAudio.clip = deathClip;
        playerAudio.Play ();

        playerMovement.enabled = false;
        //playerShooting.enabled = false;
    }


    public void RestartLevel ()
    {
        SceneManager.LoadScene (0);
    }
}
